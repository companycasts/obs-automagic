@echo off
title CompanyCast OBS Updater

set cc_obs_home=%cd%
set path=%cc_obs_home%\git\cmd;%path%

git fetch --all
git checkout stable
git checkout obs/config/obs-studio/basic/profiles/Standard
git checkout obs/config/obs-studio/basic/scenes/Standard.*
git stash
git pull --rebase
git stash pop

cd .\obs\bin\64bit
start obs64.exe

@echo off
title CompanyCast OBS Setup

echo Setup starting. This might take up to 5 minutes depending on you internet bandwidth.

set cc_obs_home=%cd%
set path=%cc_obs_home%\git\cmd;%path%

if not exist ".git\" (
	git init
	git remote add origin https://bitbucket.org/companycasts/obs-automagic.git
	git fetch --all
	git add -A
	git checkout stable
	git pull --rebase
)

echo Setup completeted successfully...
pause

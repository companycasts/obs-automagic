@echo off
title CompanyCast OBS Initializer

set cc_obs_home=%cd%
set path=%cc_obs_home%\git\cmd;%path%

git checkout obs/config/obs-studio/basic/profiles/Standard
git checkout obs/config/obs-studio/basic/scenes/Standard.*

cd .\obs\bin\64bit
start obs64.exe

# CompanyCast OBS Automagic
This is a repo intended to distribute and manage OBS Studio among clients of CompanyCast AS.

## Setup
1. Visit https://bitbucket.org/companycasts/obs-automagic/src/master/ and download the repository.
2. Unpack the repository to C:\Users\<user>\companycast-obs.
3. Run the script "setup.bat" and wait for it to complete.
4. Create a shortcut to "obs.bat" and place it somewhere accessible on the computer. I.E the Start Menu or Desktop.
5. Create a shortcut to "on_boot.bat" and place it in C:\Users\<user>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup.
6. Open OBS via the OBS shortcut file, duplicate the Profile and Scene Collection and setup the environment for the client.

## Usage
Clients should be instructed to always use the aformentioned shortcut in step 3 above when starting OBS.
If the client wants to modify OBS' settings, scenes or sources they should create their own scene collection og profile.

## TODO
Backup profile and scene collection.
Test with several changes/conflicts.
